<!DOCTYPE html>
<html>
<head>
      <meta charset="UTF-8">
      <meta name="description" content="PHP domaci - funkcije i varijabli.">
      <meta name="keywords" content="php, functions, variables">
      <meta name="author" content="Nemanja Stojanović">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">


  <title>PHP Variables And Functions Practice</title>

</head>
<body>

  <h1>PHP Variables And Functions Practice</h1>

  <p><h3>String</h3> </p>

  <?php
      $color="red";
      $string="Hello World!";
      echo "<span style='color:$color'> " .$string.  "</span>";
  ?>

  <p><h3>Integer</h3></p>

  <?php
      $color="green";
      $int=19;
      echo "<span style='color:$color'> " .$int. "</span>";
  ?>


  <p><h3>Float</h3></p>

  <?php
      $color="blue";
      $float=21.05;
      echo "<span style='color:$color'> " .$float. "</span>";
  ?>

  <p><h3>Boolean</h3></p>

  <?php
      $color="skyblue";
      $bool=TRUE;
      echo "<span style='color:$color'> " .$bool. "</span>";
  ?>

  <p><h3>Array</h3></p>

  <?php
      $color = "orange";
      $array = array("Hat, ","Scarf, ","Coat, ","Wallet, ","Boots, ", "Socks, " );
      foreach($array as $value ){
        echo "<span style='color:$color'> " .$value. "</span>";
      }
  ?>
 </div>

<div>
  <p><h3>Include Function</h3></p>

  <?php
    include 'function.php';
    $niz1= array(1, 19, 21, 55, 98, 63, 77);
    echo colorSuma('red', $niz1);
    $niz2= array(1, 98, 63, 77);
    echo colorSuma('#2d8659', $niz2);
   ?>


<p><h3>Include Function max_number</h3></p>

  <?php
    include 'maxfunction.php';
    $arrayName = array(1, 40, -21, 19, 22, 44, 66);
    max_number($arrayName, '#990066');

   ?>

   <p><h3>Include Function min_number</h3></p>

     <?php
       include 'minfunction.php';
       $arrayName = array(1, 40, -21, 19, 22, 44, 66);
       min_number($arrayName, '#990066');

      ?>



</div>





</body>
</html>
